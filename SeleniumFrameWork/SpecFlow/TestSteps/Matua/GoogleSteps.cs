﻿//using BrowserStack;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using OpenQA.Selenium;
//using OpenQA.Selenium.Remote;
//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.Configuration;
//using System.IO;
//using System.Threading;
//using TechTalk.SpecFlow;

//namespace SeleniumFrameWork.SpecFlow.TestSteps.Matua
//{
//    [Binding]
//    public class GoogleSteps
//    {
//        private IWebDriver _driver;
//        readonly BrowserStackDriver _bsDriver;

//        public GoogleSteps()
//        {
//            _bsDriver = (BrowserStackDriver)ScenarioContext.Current["bsDriver"];
//        }

//        [Given(@"I am on the google page for (.*) and (.*)")]
//        public void GivenIAmOnTheGooglePage(string profile, string environment)
//        {
//            _driver = _bsDriver.Init(profile, environment);
//            _driver.Navigate().GoToUrl("https://www.google.com/ncr");
//        }

//        [When(@"I search for ""(.*)""")]
//        public void WhenISearchFor(string keyword)
//        {
//            var q = _driver.FindElement(By.Name("q"));
//            q.SendKeys(keyword);
//            q.Submit();
//        }
        
//        [Then(@"I should see title ""(.*)""")]
//        public void ThenIShouldSeeTitle(string title)
//        {
//            Thread.Sleep(5000);
//            Assert.AreEqual(_driver.Title, title);
//        }
//    }

//    [Binding]
//    public sealed class BrowserStack
//    {
//        private BrowserStackDriver bsDriver;
//        private string[] tags;

//        [BeforeScenario]
//        public void BeforeScenario()
//        {
//            bsDriver = new BrowserStackDriver(ScenarioContext.Current);
//            ScenarioContext.Current["bsDriver"] = bsDriver;
//        }

//        [AfterScenario]
//        public void AfterScenario()
//        {
//            bsDriver.Cleanup();
//        }
//    }

//    public class BrowserStackDriver
//    {
//        private IWebDriver driver;
//        private Local browserStackLocal;
//        private string profile;
//        private string environment;
//        private ScenarioContext context;

//        public BrowserStackDriver(ScenarioContext context)
//        {
//            this.context = context;
//        }

//        public IWebDriver Init(string profile, string environment)
//        {
//            NameValueCollection caps = ConfigurationManager.GetSection("capabilities/" + profile) as NameValueCollection;
//            NameValueCollection settings = ConfigurationManager.GetSection("environments/" + environment) as NameValueCollection;

//            DesiredCapabilities capability = new DesiredCapabilities();

//            foreach (string key in caps.AllKeys)
//            {
//                capability.SetCapability(key, caps[key]);
//            }

//            foreach (string key in settings.AllKeys)
//            {
//                capability.SetCapability(key, settings[key]);
//            }

//            String username = Environment.GetEnvironmentVariable("BROWSERSTACK_USERNAME");
//            if (username == null)
//            {
//                username = ConfigurationManager.AppSettings.Get("user");
//            }

//            String accesskey = Environment.GetEnvironmentVariable("BROWSERSTACK_ACCESS_KEY");
//            if (accesskey == null)
//            {
//                accesskey = ConfigurationManager.AppSettings.Get("key");
//            }

//            capability.SetCapability("browserstack.user", username);
//            capability.SetCapability("browserstack.key", accesskey);

//            //File.AppendAllText("C:\\Users\\Admin\\Desktop\\sf.log", "Starting local");

//            if (capability.GetCapability("browserstack.local") != null && capability.GetCapability("browserstack.local").ToString() == "true")
//            {
//                browserStackLocal = new Local();
//                List<KeyValuePair<string, string>> bsLocalArgs = new List<KeyValuePair<string, string>>() {
//        new KeyValuePair<string, string>("key", accesskey)
//      };
//                browserStackLocal.start(bsLocalArgs);
//            }

//            //File.AppendAllText("C:\\Users\\Admin\\Desktop\\sf.log", "Starting driver");
//            driver = new RemoteWebDriver(new Uri("http://" + ConfigurationManager.AppSettings.Get("server") + "/wd/hub/"), capability);
//            return driver;
//        }

//        public void Cleanup()
//        {
//            driver.Quit();
//            if (browserStackLocal != null)
//            {
//                browserStackLocal.stop();
//            }
//        }
//    }
//}
