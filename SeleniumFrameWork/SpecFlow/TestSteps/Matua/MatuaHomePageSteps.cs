﻿using SeleniumFrameWork.Common;
using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace SeleniumFrameWork.SpecFlow.TestSteps.Matua
{   

    [Binding]
    public class MatuaHomePageSteps
    {
        static void Browserstack()
        {
            IWebDriver driver;
            DesiredCapabilities capability = new DesiredCapabilities();
            capability.SetCapability("browserName", "iPhone");
            capability.SetCapability("device", "iPhone 8 Plus");
            capability.SetCapability("realMobile", "true");
            capability.SetCapability("os_version", "11.0");
            capability.SetCapability("browserstack.user", "loudtester1");
            capability.SetCapability("browserstack.key", "NqdwoSAFBQxhVLPLgkz7");

            driver = new RemoteWebDriver(
              new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
            );
            driver.Navigate().GoToUrl("http://www.google.com");
            Console.WriteLine(driver.Title);

            IWebElement query = driver.FindElement(By.Name("q"));
            query.SendKeys("Browserstack");
            query.Submit();
            Console.WriteLine(driver.Title);

            driver.Quit();
        }
        [Given(@"user opens Matua home page")]
        public void GivenUserOpensMatuaHomePage()
        {            
            Initialiser.Pages.MatuaHomePage.ConfirmCurrentPage();
        }
        
        [Then(@"the Matua logo is displayed")]
        public void ThenTheMatuaLogoIsDisplayed()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"the Matua Homepage is displayed")]
        public void ThenTheMatuaHomepageIsDisplayed()
        {
            
            var matuaLeekCreamCheeseQuicheReceipe1 = Initialiser.Pages.MatuaHomePage.MatuaLeekCreamCheeseQuicheReceipe1;
            var matuaLeekCreamCheeseQuicheReceipe = Initialiser.Pages.MatuaHomePage.MatuaLeekCreamCheeseQuicheReceipe.Text;
        }

    }
}
