﻿using SeleniumFrameWork.Common;
using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumFrameWork.Pages;
using SeleniumFrameWork.DataStructures;
using System.Collections.Generic;
using System.Drawing.Imaging;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Support.Extensions;
using System.Drawing;
using WDSE.Decorators;
using WDSE.ScreenshotMaker;
using WDSE;
using System.IO;
using System.Configuration;
using TechTalk.SpecFlow.Tracing;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
//using DotRez.Utilities.WebDriver;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using OpenQA.Selenium.Support.UI;
using System.Reflection;

namespace SeleniumFrameWork.SpecFlow.TestSteps.LandmarkClientPortal
{
    
    [Binding]
    public class ClientLoginSteps
    {
        public static string SCREEN_SHOT_LOCATION = @"C:\temp\ScreenShots";

        [Given(@"user login to Landmark Client page with user name ""(.*)"" and ""(.*)""")]
        public void GivenUserLoginToLandmarkClientPageWithUserNameAnd(string userName, string password)
        {
            Initialiser.Pages.CleintLoginPage._driver.Navigate().GoToUrl(AppConfig.Urls.ClientLogin);            

            Initialiser.Pages.CleintLoginPage.UserName.SendKeys(userName);
            Initialiser.Pages.CleintLoginPage.Password.SendKeys("Landmark123");
            Initialiser.Pages.CleintLoginPage.LoginButton.Click();
            Ajax.WaitForPageToFullyLoad(true);

            ScenarioContext.Current.Add("LandmarkClientUserName", userName);

            // take only the visible area
            Screenshot ss = ((ITakesScreenshot)Initialiser.Driver).GetScreenshot();
            ss.SaveAsFile("C:\\temp\\ScreenShots\\test3.png", OpenQA.Selenium.ScreenshotImageFormat.Png);
        }


        //[Given(@"user opens Landmark Client page")]
        //public void GivenUserOpensLandmarkClientPage()
        //{
        //    Initialiser.Pages.CleintLoginPage._driver.Navigate().GoToUrl(AppConfig.Urls.ClientLogin);
        //}

        //[When(@"user is logged in with the ""(.*)"" and ""(.*)""")]
        //public void WhenUserIsLoggedInWithTheAnd(string userName, string password)
        //{            
        //    Initialiser.Pages.CleintLoginPage._driver.Navigate().GoToUrl(AppConfig.Urls.ClientLogin);

        //    Initialiser.Pages.CleintLoginPage.UserName.SendKeys(userName);
        //    Initialiser.Pages.CleintLoginPage.Password.SendKeys("Welcome123");
        //    Initialiser.Pages.CleintLoginPage.LoginButton.Click();
        //    Ajax.WaitForPageToFullyLoad(true);

        //    // take only the visible area
        //    Screenshot ss = ((ITakesScreenshot)Initialiser.Driver).GetScreenshot();
        //    ss.SaveAsFile("C:\\temp\\ScreenShots\\test3.png", OpenQA.Selenium.ScreenshotImageFormat.Png);
        //    //TakingHTML2CanvasFullPageScreenshot();

        //    //var vcs = new VerticalCombineDecorator(new ScreenshotMaker());
        //    //var screen = Initialiser.Driver.TakeScreenshot(vcs);

        //    //var bytesArr = Initialiser.Driver.TakeScreenshot(new VerticalCombineDecorator(new ScreenshotMaker()));

        //    //using (FileStream stream = new FileStream(@"C:\temp\ScreenShots\test1.png", FileMode.Create, FileAccess.ReadWrite))
        //    //{
        //    //    stream.Write(screen,0,screen.Length);
        //    //    stream.Close();
        //    //}
        //    //TakeSnapshot();

        //}

        [Then(@"Landmark client dashboard is displayed")]
        public void ThenLandmarkClientDashboardIsDisplayed()
        {
            var topRibbonMyLandmarkLinkLable = Initialiser.Pages.CleintLoginPage.MyLandmarkLink.Text;
            //var topRibbonWoolClipLinkLable;
            AssertAndLogMessages assertLogMessage = new AssertAndLogMessages
            {
                LogCategory = new List<string>() { TestReportCategories.SANITY_TEST },
                ScenarioName = "01. Login page - Verify that user can successfully login",
                Condition = topRibbonMyLandmarkLinkLable == "My Landmark",
            };
            ReportingLogMessages.AssertAndLog(assertLogMessage);
        }

        [AfterScenario(new string[] { })]
        public static void AfterScenario()
        {
            Initialiser.Quit();
        }


        public void TakingHTML2CanvasFullPageScreenshot()
        {


            Initialiser.Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);
            //Initialiser.Driver.Navigate().GoToUrl(@"https://automatetheplanet.com");
                IJavaScriptExecutor js = (IJavaScriptExecutor)Initialiser.Driver;
                var html2canvasJs = File.ReadAllText($"{GetAssemblyDirectory()}html2canvas.js");
                js.ExecuteScript(html2canvasJs);
                string generateScreenshotJS = @"function genScreenshot () {
	                                        var canvasImgContentDecoded;
	                                        html2canvas(document.body, {
 		                                        onrendered: function (canvas) {                                          
		                                            window.canvasImgContentDecoded = canvas.toDataURL(""image/png"");
	                                        }});
                                        }
                                        genScreenshot();";
                js.ExecuteScript(generateScreenshotJS);
                var wait = new WebDriverWait(Initialiser.Driver, TimeSpan.FromSeconds(10));
                wait.IgnoreExceptionTypes(typeof(InvalidOperationException));
                wait.Until(
                    wd =>
                    {
                        string response = (string)js.ExecuteScript
                            ("return (typeof canvasImgContentDecoded === 'undefined' || canvasImgContentDecoded === null)");
                        if (string.IsNullOrEmpty(response))
                        {
                            return false;
                        }
                        return bool.Parse(response);
                    });
                wait.Until(wd => !string.IsNullOrEmpty((string)js.ExecuteScript("return canvasImgContentDecoded;")));
                var pngContent = (string)js.ExecuteScript("return canvasImgContentDecoded;");
                pngContent = pngContent.Replace("data:image/png;base64,", string.Empty);
                byte[] data = Convert.FromBase64String(pngContent);
                var tempFilePath = Path.GetTempFileName().Replace(".tmp", ".png");
                Image image;
                using (var ms = new MemoryStream(data))
                {
                    image = Image.FromStream(ms);
                }
            //image.Save(tempFilePath, ImageFormat.Png);
            image.Save("C:\\temp\\ScreenShots\\", ImageFormat.Png);

        }

        private string GetAssemblyDirectory()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }


        //public static void TakeSnapshot()
        //{

        //    IJavaScriptExecutor js = Initialiser.Driver as IJavaScriptExecutor;
        //    var fileName = ScenarioContext.Current.ScenarioInfo.Title.ToIdentifier() + DateTime.Now.ToString("HH_mm_ss") + "JS" + ".png";
        //    var fileLocation = Path.Combine(SCREEN_SHOT_LOCATION, fileName);
        //    Image finalImage;
        //    // get the full page height and current browser height
        //    string getCurrentBrowserSizeJS =
        //        @"

        //        window.browserHeight = (window.innerHeight || document.body.clientHeight);
        //        window.headerHeight= document.getElementById('site-header').clientHeight;;
        //        window.fullPageHeight = document.body.scrollHeight;
        //    ";
        //    js.ExecuteScript(getCurrentBrowserSizeJS);


        //    // * This is async operation. So we have to wait until it is done.
        //    string getSizeHeightJS = @"return window.browserHeight;";
        //    int contentHeight = 0;
        //    while (contentHeight == 0)
        //    {
        //        contentHeight = Convert.ToInt32(js.ExecuteScript(getSizeHeightJS));
        //        if (contentHeight == 0) System.Threading.Thread.Sleep(10);
        //    }

        //    string getContentHeightJS = @"return window.headerHeight;";
        //    int siteHeaderHeight = 0;
        //    while (siteHeaderHeight == 0)
        //    {
        //        siteHeaderHeight = Convert.ToInt32(js.ExecuteScript(getContentHeightJS));
        //        if (siteHeaderHeight == 0) System.Threading.Thread.Sleep(10);
        //    }

        //    string getFullPageHeightJS = @"return window.fullPageHeight";
        //    int fullPageHeight = 0;
        //    while (fullPageHeight == 0)
        //    {
        //        fullPageHeight = Convert.ToInt32(js.ExecuteScript(getFullPageHeightJS));
        //        if (fullPageHeight == 0) System.Threading.Thread.Sleep(10);
        //    }

        //    if (contentHeight == fullPageHeight)
        //    {
        //        TakeSnapshotCurrentPage();

        //    }
        //    else
        //    {
        //        int scollEachHeight = contentHeight - siteHeaderHeight;
        //        int shadowAndBorder = 3;
        //        int scollCount = 0;
        //        int existsIf = (fullPageHeight - siteHeaderHeight) % scollEachHeight;
        //        bool cutIf = true;

        //        if (existsIf == 0)
        //        {
        //            scollCount = (fullPageHeight - siteHeaderHeight) / scollEachHeight;
        //            cutIf = false;
        //        }
        //        else
        //        {
        //            scollCount = (fullPageHeight - siteHeaderHeight) / scollEachHeight + 1;
        //            cutIf = true;
        //        }


        //        // back to top start screenshot
        //        string scollToTopJS = "window.scrollTo(0, 0)";
        //        js.ExecuteScript(scollToTopJS);

        //        Byte[] imageBaseContent = ((ITakesScreenshot)Initialiser.Driver).GetScreenshot().AsByteArray;
        //        Image imageBase;
        //        using (var ms = new MemoryStream(imageBaseContent))
        //        {
        //            imageBase = Image.FromStream(ms);
        //        }

        //        finalImage = imageBase;

        //        string scrollBar = @"window.scrollBy(0, window.browserHeight-window.headerHeight);";
        //        for (int count = 1; count < scollCount; count++)
        //        {

        //            js.ExecuteScript(scrollBar);
        //            Thread.Sleep(500);
        //            Byte[] imageContentAdd = ((ITakesScreenshot)Initialiser.Driver).GetScreenshot().AsByteArray;
        //            Image imageAdd;
        //            using (var msAdd = new MemoryStream(imageContentAdd))
        //            {
        //                imageAdd = Image.FromStream(msAdd);
        //            }

        //            imageAdd.Save(fileLocation, ImageFormat.Png);
        //            Bitmap source = new Bitmap(imageAdd);
        //            int a = imageAdd.Width;
        //            int b = imageAdd.Height - siteHeaderHeight;
        //            PixelFormat c = source.PixelFormat;


        //            // cut the last screen shot if last screesshot override with sencond last one
        //            if ((count == (scollCount - 1)) && cutIf)
        //            {

        //                Bitmap imageAddLastCut =
        //                    source.Clone(new System.Drawing.Rectangle(0, contentHeight - existsIf, imageAdd.Width, existsIf), source.PixelFormat);

        //                finalImage = combineImages(finalImage, imageAddLastCut);

        //                source.Dispose();
        //                imageAddLastCut.Dispose();
        //            }
        //            //cut the site header from screenshot
        //            else
        //            {
        //                Bitmap imageAddCutHeader =
        //                    source.Clone(new System.Drawing.Rectangle(0, (siteHeaderHeight + shadowAndBorder), imageAdd.Width, (imageAdd.Height - siteHeaderHeight - shadowAndBorder)), source.PixelFormat);
        //                finalImage = combineImages(finalImage, imageAddCutHeader);

        //                source.Dispose();
        //                imageAddCutHeader.Dispose();
        //            }

        //            imageAdd.Dispose();

        //        }

        //        finalImage.Save(fileLocation, ImageFormat.Png);
        //        imageBase.Dispose();
        //        finalImage.Dispose();

        //    }
        //}

        ////combine two pictures
        //public static Bitmap combineImages(Image image1, Image image2)
        //{
        //    Bitmap bitmap = new Bitmap(image1.Width, image1.Height + image2.Height);
        //    using (Graphics g = Graphics.FromImage(bitmap))
        //    {
        //        g.DrawImage(image1, 0, 0);
        //        g.DrawImage(image2, 0, image1.Height);
        //    }

        //    return bitmap;
        //}

        //// take current window screenshot  
        //private static void TakeSnapshotCurrentPage()
        //{
        //    try
        //    {
        //        var screenshot = ((ITakesScreenshot)Initialiser.Driver).GetScreenshot();
        //        var urlStr = Initialiser.Driver.Url;
        //        var fileName = ScenarioContext.Current.ScenarioInfo.Title.ToIdentifier() + DateTime.Now.ToString("HH_mm_ss") + ".png";
        //        var fileLocation = Path.Combine(SCREEN_SHOT_LOCATION, fileName);

        //        if (!Directory.Exists(SCREEN_SHOT_LOCATION))
        //        {
        //            Directory.CreateDirectory(SCREEN_SHOT_LOCATION);
        //        }

        //        screenshot.SaveAsFile(fileLocation, ScreenshotImageFormat.Png);
        //        Console.WriteLine(urlStr);
        //        Console.WriteLine("SCREENSHOT[{0}]SCREENSHOT", Path.Combine("Screenshots", fileName));
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }
        //}



    }
}
