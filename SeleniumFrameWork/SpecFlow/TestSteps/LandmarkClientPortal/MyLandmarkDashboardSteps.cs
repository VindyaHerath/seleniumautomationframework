﻿using SeleniumFrameWork.Common;
using SeleniumFrameWork.DataStructures;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace SeleniumFrameWork.SpecFlow.TestSteps.LandmarkClientPortal
{
    [Binding]
    public class MyLandmarkDashboardSteps
    {
        [Given(@"that Landmark user is shown the the My Landmark Dashboard")]
        public void GivenThatLandmarkUserIsShownTheTheMyLandmarkDashboard()
        {
            
        }
        
        [Then(@"Patient primary account details are displayed on the dashboard")]
        public void ThenPatientPrimaryAccountDetailsAreDisplayedOnTheDashboard()
        {
            var landMarkAccountdetail = Initialiser.Pages.MyLandmarkDashboardPage.PrimaryAccNo.Text;
            var landMarkAccountNo = landMarkAccountdetail.Substring(0,landMarkAccountdetail.IndexOf('\r'));
            
            var userName = ScenarioContext.Current.Get<string>("LandmarkClientUserName");
            var loggedInAccontNo = string.Concat("ACCOUNT NO — ", userName);

            AssertAndLogMessages assertLogMessage = new AssertAndLogMessages
            {
                LogCategory = new List<string>() { TestReportCategories.SANITY_TEST },
                ScenarioName = "02. Landmark Client dashboard - Verify that MyLandmark dashboard displayes Primary Account number",
                Condition = landMarkAccountNo == loggedInAccontNo,
            };
            ReportingLogMessages.AssertAndLog(assertLogMessage);
        }        
    }
}
