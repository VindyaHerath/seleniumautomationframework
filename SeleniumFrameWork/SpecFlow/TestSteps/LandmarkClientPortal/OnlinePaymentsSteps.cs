﻿using SeleniumFrameWork.Common;
using SeleniumFrameWork.DataStructures;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace SeleniumFrameWork.SpecFlow.TestSteps.LandmarkClientPortal
{
    [Binding]
    public class OnlinePaymentsSteps
    {
        [Given(@"that the MyLandMark user opens Online Payments page")]
        public void GivenThatTheMyLandMarkUserOpensOnlinePaymentsPage()
        {
            Initialiser.Pages.CleintLoginPage._driver.Navigate().GoToUrl(AppConfig.Urls.OnlinePaymentsPage);
        }
        
        [Then(@"verify that page title is displayed")]
        public void ThenVerifyThatPageTitleIsDisplayed()
        {
            var onlinePaymentpagetitle = Initialiser.Pages.OnlinePaymentsPage.PageTitle.Text;
            //var topRibbonWoolClipLinkLable;
            AssertAndLogMessages assertLogMessage = new AssertAndLogMessages
            {
                LogCategory = new List<string>() { TestReportCategories.SANITY_TEST },
                ScenarioName = "01. Login page - Verify page title is displayed when Online payments page is opened",
                Condition = onlinePaymentpagetitle == "Online Payments",
            };
            ReportingLogMessages.AssertAndLog(assertLogMessage);
        }
    }
}
