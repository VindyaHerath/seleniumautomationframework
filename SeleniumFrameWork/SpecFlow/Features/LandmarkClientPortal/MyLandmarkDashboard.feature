﻿Feature: MyLandmarkDashboard
	Verify that when user is logged in to landmark, her landmark Primary account details are displayed

Background: 
Given user login to Landmark Client page with user name "70260142" and "Passord"

@SanityTest
Scenario: Verify that MyLandmark dashboard displayes Primary Account number
	Given that Landmark user is shown the the My Landmark Dashboard	
	Then Patient primary account details are displayed on the dashboard
