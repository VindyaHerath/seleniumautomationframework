﻿using System;
using TechTalk.SpecFlow;

namespace SeleniumFrameWork
{
    [Binding]
    public class HomePageSteps
    {
        [Given(@"the user opens ""(.*)"" home page")]
        public void GivenTheUserOpensHomePage(string p0)
        {
            ScenarioContext.Current.Pending();
        }
    }
}
