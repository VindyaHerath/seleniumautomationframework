﻿using System;
using System.IO;
using System.Collections.Generic;


namespace SeleniumFrameWork
{
    public static class AppConfig
    {
        public static string Browser { get { return System.Configuration.ConfigurationManager.AppSettings["Browser"]; } }
        public static string Url { get { return System.Configuration.ConfigurationManager.AppSettings["Url"]; } }
        public static string SeleniumDriverPath { get { return System.Configuration.ConfigurationManager.AppSettings["SeleniumDriverPath"]; } }
        public static string TestDataPath { get { return System.Configuration.ConfigurationManager.AppSettings["TestDataPath"]; } }
        public static string ConnectionString { get { return System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]; } }

        private static string _ScreenshotFilePath;
        public static string ScreenshotFilePath
        {
            get
            {
                if (_ScreenshotFilePath == null)
                {
                    var driverParent = new DirectoryInfo(System.Configuration.ConfigurationManager.AppSettings["SeleniumDriverPath"]).Parent.FullName;
                    _ScreenshotFilePath = Path.Combine(Path.Combine(driverParent, "Screenshots"), DateTime.Now.ToString("yyyyMMddHHss"));
                }
                return _ScreenshotFilePath;
            }
        }

        public static class Urls
        {
            public static string Default { get { return GetUrl("Default.aspx"); } }

            //Test
            public static string GoogleHomePage { get { return GetUrl("www.google.com"); } }
            public static string MatuaHomePage { get { return GetUrl("en-au"); } }

            //Landmark
            public static string ClientLogin { get { return GetUrl("auth/signin"); } }
            public static string MyLandmarkDashboardPage { get { return GetUrl(""); } }
            public static string OnlinePaymentsPage { get { return GetUrl("payments/payable"); } }

            private static string GetUrl(string page)
            {
                if (AppConfig.Url.EndsWith("/"))
                {
                    return String.Format("{0}{1}", AppConfig.Url, page);
                }
                else
                {
                    return String.Format("{0}/{1}", AppConfig.Url, page);
                }
            }


        }
    }
}
