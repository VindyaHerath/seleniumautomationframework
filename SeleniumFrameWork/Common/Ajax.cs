﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumFrameWork.Common
{
    public static class Ajax
    {
        /// <summary>
        /// window.performance gives us the ability to figure out if a page has loaded
        /// all the resources it needs - slows down the tests a little, but seems
        /// to make them all work which is the important thing
        /// </summary>
        public static void WaitForPageToFullyLoad(bool secondTry = false)
        {
            var driver = Initialiser.Driver;
            int timeout = 5000;

            Stopwatch watch = new Stopwatch();
            watch.Start();

            int count = 0;
            while (true)
            {
                // so we can break in one place in case we want to add some additional logic
                // e.g. might be nice to record how long we're waiting
                bool timeToBreak = watch.ElapsedMilliseconds > timeout;

                string js = @"var pe = window.performance.getEntries();
                            var count = 0;
                            for (var i = 0; i < pe.length; i++) {
                                if (pe[i].responseEnd && pe[i].responseEnd > 0){
                                    count++;
                                }
                            } return (pe.length === count);";

                var x = (driver as IJavaScriptExecutor).ExecuteScript(js);
                bool ready = x != null ? x.ToString().Equals("true", StringComparison.OrdinalIgnoreCase) : false;

                if (timeToBreak || ready)
                {
                    if (timeToBreak || secondTry)
                    {
                        break;
                    }
                    // just to make sure
                    Thread.Sleep(100);
                    WaitForPageToFullyLoad(secondTry: true);
                }
                else
                {
                    Thread.Sleep(100);
                }
                count++;
            }
        }

    }
}
