﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFrameWork.Common
{
    public static class Initialiser
    {
        private static IWebDriver _Driver;
        public static IWebDriver Driver
        {
            get
            {
                if (_Driver == null)
                {
                    _Driver = GetDriver();
                }
                return _Driver;
            }
        }

        public static void Quit()
        {
            if (_Driver != null)
            {
                _Driver.Dispose();
                _Driver.Quit();
                _Driver = null;
                _Pages = null;
            }
        }

        private static Common.Pages _Pages;
        public static Common.Pages Pages
        {
            get
            {
                if (_Pages == null)
                {
                    _Pages = new Pages(Driver);
                }
                return _Pages;
            }
        }

        //private static TestData _TestData;
        //public static TestData TestData
        //{
        //    get
        //    {
        //        if (_TestData == null)
        //        {
        //            var configuredPath = AppConfig.TestDataPath;
        //            var path = Path.Combine(Path.GetDirectoryName(typeof(AppConfig).Assembly.Location), "TestData", configuredPath);

        //            if (configuredPath.StartsWith("file://"))
        //            {
        //                path = configuredPath.Replace("file://", string.Empty).Trim();
        //            }

        //            _TestData = new TestData(path);
        //        }
        //        return _TestData;
        //    }
        //}



        //private static RegressionTests _Regression;
        //public static RegressionTests Regression
        //{
        //    get
        //    {
        //        if (_Regression == null)
        //        {
        //            _Regression = new RegressionTests(Pages);
        //        }
        //        return _Regression;
        //    }
        //}

        //private static TestData _TestData;
        //public static TestData TestData
        //{
        //    get
        //    {
        //        if (_TestData == null)
        //        {
        //            _TestData = new TestData(Path.Combine(AssemblyDirectory, "TestData/TestData.json"));
        //        }
        //        return _TestData;
        //    }
        //}

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        private static IWebDriver GetDriver()
        {
            IWebDriver driver = null;

            if (AppConfig.Browser.Equals("Chrome", StringComparison.OrdinalIgnoreCase))
            {
                driver = new ChromeDriver(AppConfig.SeleniumDriverPath);
            }
            else if (AppConfig.Browser.Equals("IE", StringComparison.OrdinalIgnoreCase))
            {
                driver = new InternetExplorerDriver(@"C:\Data\AutomationFrameWork\SeleniumFrameWork\SeleniumDriver");
                //driver.Navigate().GoToUrl("http://www.google.com");
                var options = new InternetExplorerOptions();
                options.IgnoreZoomLevel = true;
                options.IntroduceInstabilityByIgnoringProtectedModeSettings = true; // TODO: test with this option, remove if unstable

                //driver = new InternetExplorerDriver(AppConfig.SeleniumDriverPath, options);
            }
            else if (AppConfig.Browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase))
            {
                FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Data\AutomationFrameWork\SeleniumFrameWork\SeleniumDriver"); // location of the geckdriver.exe file

                driver = new FirefoxDriver(service);
                //driver.Navigate().GoToUrl("https://google.com/");
                //var ffBinary = new FirefoxBinary(AppConfig.SeleniumDriverPath);
                //var ffProfile = new FirefoxProfile();

                //driver = new FirefoxDriver(ffBinary, ffProfile);
            }
            else if(AppConfig.Browser.Equals("Edge", StringComparison.OrdinalIgnoreCase))
            {
                //var options = new EdgeOptions()
                //{
                //    PageLoadStrategy = EdgePageLoadStrategy.Eager
                //};
                //driver = new EdgeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), options);
                driver = new EdgeDriver();
            }
            else
            {
                throw new Exception(String.Format("Browser '{0}' not supported", AppConfig.Browser));
            }

            return driver;
        }


        public static void CloseBrowser()
        {
            try
            {
                //remove the test data
                _Driver.Close();
            }
            finally
            {
                _Driver = null;
            }
        }


        //internal static void SwitchOutOfIFrame()
        //{
        //    throw new NotImplementedException();
        //}


        public static void SwitchOutOfIFrame(this IWebDriver driver)
        {
            driver.SwitchTo().DefaultContent();
        }
    }
}
