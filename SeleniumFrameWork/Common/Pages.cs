﻿using OpenQA.Selenium;
using SeleniumFrameWork.Pages;
using SeleniumFrameWork.Pages.LandmarkClient;
using SeleniumFrameWork.Pages.Matua;
using System;

namespace SeleniumFrameWork.Common
{
    public class Pages
    {
        private IWebDriver _driver;

        public Pages(IWebDriver driver)
        {
            this._driver = driver;
        }


        private DefaultPage _DefaultPage;
        public DefaultPage DefaultPage
        {
            get { if (_DefaultPage == null) _DefaultPage = new DefaultPage(this._driver); return _DefaultPage; }
            set { _DefaultPage = value; }
        }

        private GoogleHomePage _GoogleHomePage;
        public GoogleHomePage GoogleHomePage
        {
            get { if (_GoogleHomePage == null) _GoogleHomePage = new GoogleHomePage(this._driver); return _GoogleHomePage; }
            set { _GoogleHomePage = value; }
        }

        

        private MatuaHomePage _MatuaHomePage;
        public MatuaHomePage MatuaHomePage
        {
            get { if (_MatuaHomePage == null) _MatuaHomePage = new MatuaHomePage(this._driver); return _MatuaHomePage; }
            set { _MatuaHomePage = value; }
        }


        //Landmark Client portal
        private CleintLoginPage _CleintLoginPage;
        public CleintLoginPage CleintLoginPage
        {
            get { if (_CleintLoginPage == null) _CleintLoginPage = new CleintLoginPage(this._driver); return _CleintLoginPage; }
            set { _CleintLoginPage = value; }
        }

        private MyLandmarkDashboardPage _MyLandmarkDashboardPage;
        public MyLandmarkDashboardPage MyLandmarkDashboardPage
        {
            get { if (_MyLandmarkDashboardPage == null) _MyLandmarkDashboardPage = new MyLandmarkDashboardPage(this._driver); return _MyLandmarkDashboardPage; }
            set { _MyLandmarkDashboardPage = value; }
        }

        private OnlinePaymentsPage _OnlinePaymentsPage;
        public OnlinePaymentsPage OnlinePaymentsPage
        {
            get { if (_OnlinePaymentsPage == null) _OnlinePaymentsPage = new OnlinePaymentsPage(this._driver); return _OnlinePaymentsPage; }
            set { _OnlinePaymentsPage = value; }
        }

        /// <summary>
        /// if one test fails, we don't want the next one to fail, so this is called before
        /// every test to make sure the current page is where the current test needs to be
        /// </summary>
        public static void ConfirmCurrentPage(string expectedUrl, IWebDriver driver)
        {
            // selenium doesn't like going from HTTPS to HTTP
            if (driver.Url.StartsWith("https") && !expectedUrl.StartsWith("https"))
            {
                expectedUrl = expectedUrl.Replace("http", "https");
            }

            

            if (!expectedUrl.Equals(driver.Url, StringComparison.OrdinalIgnoreCase))
            {
                driver.Navigate().GoToUrl(expectedUrl);
                //driver.Navigate().GoToUrl(driver.Url);
            }
        }

    }
}
