﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using RelevantCodes.ExtentReports;
using System.Configuration;
using SeleniumFrameWork.DataStructures;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SeleniumFrameWork.Common
{
    public static class ReportingLogMessages
    {


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Condition, LogCategory, ScenarioName"></param>
        public static void AssertAndLog(AssertAndLogMessages assertLogMessage)
        {
            ExtentReports _exRepo;
            ExtentTest _logs;

            var reportpath = ConfigurationManager.AppSettings["Reports"].ToString();
            _exRepo = new ExtentReports(reportpath, false, DisplayOrder.NewestFirst);
            _logs = _exRepo.StartTest(assertLogMessage.ScenarioName);

            try
            {
                if (assertLogMessage.Condition)
                {
                    //Assert.True(assertLogMessage.Condition, assertLogMessage.ScenarioName);
                    //Logger.WriteLog(LogLevelL4N.INFO, "Successfully tested");
                    _logs.AssignCategory(assertLogMessage.LogCategory.ToArray());
                    _logs.Log(LogStatus.Pass, assertLogMessage.ScenarioName);
                }
                else
                {
                    //Assert.True(false);
                    //Logger.WriteLog(LogLevelL4N.INFO, "Test failed");
                    _logs.AssignCategory(assertLogMessage.LogCategory.ToArray());
                    _logs.Log(LogStatus.Fail, assertLogMessage.ScenarioName);
                }
            }
            catch (Exception e)
            {
                //Logger.WriteLog(LogLevelL4N.INFO, "Test failed");
                _logs.AssignCategory(assertLogMessage.LogCategory.ToArray());
                _logs.Log(LogStatus.Fail, assertLogMessage.ScenarioName);
                //throw e;
            }
            finally
            {
                _exRepo.EndTest(_logs);
                _exRepo.Flush();
            }
        }

    }
}
