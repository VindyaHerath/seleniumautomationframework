﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFrameWork.Pages
{
    public interface IConfirmCurrentPage
    {
        /// <summary>
        /// if one test fails, we don't want the next one to fail, so this is called before
        /// every test to make sure the current page is where the current test needs to be
        /// </summary>
        void ConfirmCurrentPage();
    }
}
