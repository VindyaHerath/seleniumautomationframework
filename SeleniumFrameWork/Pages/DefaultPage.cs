﻿using OpenQA.Selenium;


namespace SeleniumFrameWork.Pages
{
    public class DefaultPage : PageBase, IConfirmCurrentPage
    {//In the construtor pass the selenium WebDriver
        public DefaultPage(IWebDriver driver) : base(driver) { }

        public void ConfirmCurrentPage()
        {
            Common.Pages.ConfirmCurrentPage(AppConfig.Urls.Default, this._driver);

        }

        public IWebElement SignIn
        {
            get
            {
                return base.Find(By.LinkText("Sign"));
            }
        }
        
    }
}
