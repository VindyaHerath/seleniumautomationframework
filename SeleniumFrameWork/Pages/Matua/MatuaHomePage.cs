﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using SeleniumFrameWork.Pages;

namespace SeleniumFrameWork.Pages.Matua
{
    public class MatuaHomePage : PageBase, IConfirmCurrentPage
    {
        public MatuaHomePage(IWebDriver driver) : base(driver) { }

        public void ConfirmCurrentPage()
        {
            Common.Pages.ConfirmCurrentPage(AppConfig.Urls.MatuaHomePage, this._driver);
        }

        public IWebElement MatuaLeekCreamCheeseQuicheReceipe
        {
            get { return this.Find(By.Id("phmain_0_phcontent_2_ImageLeftContent_ContentWithImage_ContentExtractWrapper")); }
        }

        public IWebElement MatuaLeekCreamCheeseQuicheReceipe1
        {
            get { return this.Find(By.XPath(".//*[@id='phmain_0_phcontent_2_ImageLeftContent_ContentWithImage_ContentWithImageTextWrapper']/h2[1]")); }
        }
    }
}
