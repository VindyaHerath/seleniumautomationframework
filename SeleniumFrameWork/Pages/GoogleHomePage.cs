﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFrameWork.Pages
{
    public class GoogleHomePage : PageBase, IConfirmCurrentPage
    {
        public GoogleHomePage(IWebDriver driver) : base(driver) { }

        public void ConfirmCurrentPage()
        {
            Common.Pages.ConfirmCurrentPage(AppConfig.Urls.GoogleHomePage, this._driver);
        }

        public IWebElement AppointmentTable
        {
            get { return this.Find(By.Id("ctl00_ContentPlaceHolder1_templateControl_WCZ3_2_Appointment_Table_V2Zone1_2_3_gvContentList")); }
        }

    }
}
