﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFrameWork.Pages.LandmarkClient
{
    public class CleintLoginPage: PageBase, IConfirmCurrentPage
    {
        public CleintLoginPage(IWebDriver driver) : base(driver) { }

        public void ConfirmCurrentPage()
        {
            Common.Pages.ConfirmCurrentPage(AppConfig.Urls.ClientLogin, this._driver);
        }

        public IWebElement LoginTitle
        {
            get { return this.Find(By.ClassName("font-1 font-stretch-condensed font-semibold text-5xl text-neutral-2 mb-4 md:mb-6 text-left")); }
        }

        public IWebElement UserName
        {
            get { return this.Find(By.Id("username")); }
        }

        public IWebElement Password
        {
            get { return this.Find(By.Id("password")); }
        }

        public IWebElement LoginButton
        {
            get { return this.Find(By.ClassName("form-btn")); }
        }

        public IWebElement MyLandmarkLink
        {
            get { return this.Find(By.LinkText("My Landmark")); }
        }
    }
}
