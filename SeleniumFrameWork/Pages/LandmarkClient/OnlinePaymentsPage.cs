﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFrameWork.Pages.LandmarkClient
{
    public class OnlinePaymentsPage: PageBase, IConfirmCurrentPage
    {
        public OnlinePaymentsPage(IWebDriver driver) : base(driver) { }

        public void ConfirmCurrentPage()
        {
            Common.Pages.ConfirmCurrentPage(AppConfig.Urls.OnlinePaymentsPage, this._driver);
        }

        public IWebElement PageTitle
        {
            get { return this.Find(By.XPath("//*[@id='app']/div[1]/div[2]/div/div[2]/div[2]/div/div/div[1]/h1")); }
        }
    }
}
