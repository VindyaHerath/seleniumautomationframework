﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFrameWork.Pages.LandmarkClient
{
    public class MyLandmarkDashboardPage: PageBase, IConfirmCurrentPage
    {
        public MyLandmarkDashboardPage(IWebDriver driver) : base(driver) { }

        public void ConfirmCurrentPage()
        {
            Common.Pages.ConfirmCurrentPage(AppConfig.Urls.MyLandmarkDashboardPage, this._driver);
        }

        public IWebElement PrimaryAccNo
        {
            get { return this.Find(By.ClassName("tile-info__title-block")); }
        }
    }
}
