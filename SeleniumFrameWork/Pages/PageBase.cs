﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using SeleniumFrameWork.Pages.Matua;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace SeleniumFrameWork.Pages
{
    public class PageBase
    {        
        public IWebDriver _driver { private set; get; }


        
        public PageBase(IWebDriver driver)
        {
            _driver = driver;
            _driver.Manage().Window.Maximize();

            // in case an element doesn't show up when expected, 
            // we can tell the driver to keep trying to find it (we're setting timeout to 30sec)
            //_driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 30));
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }   

        /// <summary>
        /// True if there is a website user logged in, false otherwise
        /// </summary>
        public bool IsLoggedIn
        {
            get { return this._driver.PageSource.Contains("Logout"); }
        }

        public List<IWebElement> FindElements(By by)
        {
            List<IWebElement> elements = null;

            int timeout = 10000; // 10 seconds

            Stopwatch watch = new Stopwatch();
            watch.Start();

            // tried using the built-in Wait options, but didn't always work - doing this
            // means we don't need to put Thread.Sleep() in the tests
            while (elements == null)
            {
                if (watch.ElapsedMilliseconds > timeout)
                {
                    break;
                }

                Thread.Sleep(100);

                elements = this._driver.FindElements(by).ToList();
            }

            // take a screen shot for every action
            this._driver.TakeScreenshot();

            return elements;
        }
        /// <summary>
        /// Abtract away in case we need to add some extra logic (e.g. waiting for an element to
        /// appear if it doesn't exist right away)
        /// </summary>
        public IWebElement Find(By by)
        {
            IWebElement element = null;

            int timeout = 10000; // 10 seconds

            Stopwatch watch = new Stopwatch();
            watch.Start();

            // tried using the built-in Wait options, but didn't always work - doing this
            // means we don't need to put Thread.Sleep() in the tests
            while (element == null)
            {
                if (watch.ElapsedMilliseconds > timeout)
                {
                    break;
                }

                Thread.Sleep(100);

                element = this._driver.FindElements(by).FirstOrDefault();
            }

            // take a screen shot for every action
            this._driver.TakeScreenshot();

            return element;
        }
    }
}
