﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFrameWork.DataStructures
{
    public class AssertAndLogMessages
    {
        /// <summary>
        /// Gets or Sets Condition
        /// </summary>
        public bool Condition { get; set; }

        /// <summary>
        /// Gets or Sets LogCategory
        /// </summary>
        public IList<string> LogCategory { get; set; }

        /// <summary>
        /// Gets or Sets ScenarioName
        /// </summary>
        public string ScenarioName { get; set; }

        /// <summary>
        /// Gets or Sets ConditionMethod
        /// </summary>
        public Func<bool> ConditionMethod { get; set; }
    }

    public static class TestReportCategories
    {
        public const string SANITY_TEST = "SanityTest";
        
        public static string TranslateCategories(string resourceType)
        {
            var categories = new List<string> {
                TestReportCategories.SANITY_TEST,               
            };

            if (categories.Contains(resourceType))
                return categories.FirstOrDefault(c => c == resourceType);
            else
                throw new Exception("TEST AUTOMATION EXCEPTION : Resource Type not recognised");
        }


    }

    public enum TestReport_Categories
    {
        SANITY_TEST,       
    }
}
